#language: pt
Funcionalidade: Alterar wallpaper de um chat
  
	Eu como usuário quero poder trocar o wallpaper de um chat específico por uma imagem da minha galeria
	Uma vez que já tenha sido alterado, quero poder voltar com o wallpaper padrão

	Cenario: Adicionar uma imagem da Galeria
	Dado que estou em um chat com wallpaper padrão ou qualquer outro
	Quando Entro no menu "Walpaper"
	E seleciono a opção "Galeria"
	E seleciono a "Imagem A"
	Entao o chat deve exibir a "Imagem A" como wallpaper

	Cenario: Voltar ao wallpaper padrão 
	Dado que estou em um chat com wallpaper diferente do padrão
	Quando Entro no menu "Walpaper"
	E seleciono a opção "Default"
	Entao o chat deve exibir o "Wallpaper Padrão"
