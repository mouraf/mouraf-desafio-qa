#language: pt
Funcionalidade: Resetar estatísticas
  
	Eu como usuário quero resetar as estatísticas de trafego de dados

	Cenario: Resetar estatísticas
	Dado que estou na tela "Network Usage"
	Quando seleciono "Reset Statistics"
	E seleciono "Reset"
	Entao os campos "Messages sent", "Messages received", "Media bytes sent", "Media bytes received", 
	"Message bytes sent ", "Message bytes received", 	"Outgoing WhatsApp calls", "Incoming WhatsApp calls", 
	"WhatsApp call bytes sent", "WhatsApp call bytes received", "Total bytes sent" e "Total bytes received" devem exibir valor  '0'
	E o campo "Last reset time" deve exibir "data e hora atuais"
